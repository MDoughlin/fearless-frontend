window.addEventListener('DOMContentLoaded', async () => {

    let response = await fetch('http://localhost:8000/api/conferences/');

    const data = await response.json();
    const selecTag = document.getElementById('create-conference-form')

    for (let location of data.locations){
        let option = document.createElement('option')
        option.value = location.id
        option.innerHTML = location.name
        selectTag.appendChild(option);
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const conferenceUrl = 'http://localhost:8000/api/conferences';
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch (conferenceUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });


});
