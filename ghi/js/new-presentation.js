window.addEventListener('DOMContentLoaded', async () => {

    let response = await fetch('http://localhost:8000/api/conferences/');

    const data = await response.json();
    //i feel like this should be towards the conference
    const selectTag = document.getElementById('create-presentation-form')

    for (let conference of data.conferences){
        let option = document.createElement('option')
        option.value = conference.id
        option.innerHTML = conference.name
        selectTag.appendChild(option);
    }
    const formTag = document.getElementById('create-presentation-form')
    formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    //i need to repesent each href of the conference -_-
    const presentationUrl = 'http://localhost:8000/api/conferences/'
    const fetchConfig = {
        method: 'POST',
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    }
    const response = await fetch(presentaionUrl, fetchConfig);
    if (response.ok){
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
    }
    });
});
